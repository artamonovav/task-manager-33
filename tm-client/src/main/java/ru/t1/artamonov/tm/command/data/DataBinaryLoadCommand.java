package ru.t1.artamonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.DataBinaryLoadRequest;

public final class DataBinaryLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    private static final String DESCRIPTION = "Load data from binary file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA LOAD BINARY]");
        @NotNull DataBinaryLoadRequest request = new DataBinaryLoadRequest(getToken());
        getDomainEndpointClient().loadDataBinary(request);
    }

}
