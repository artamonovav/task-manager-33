package ru.t1.artamonov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.dto.request.DataXmlSaveFasterXmlRequest;


public final class DataXmlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private static final String NAME = "data-save-xml";

    @NotNull
    private static final String DESCRIPTION = "Save data in xml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE XML]");
        @NotNull DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(getToken());
        getDomainEndpointClient().saveDataXmlFasterXml(request);
    }

}
